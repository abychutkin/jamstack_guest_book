export const state = () => ({
    messages: []
});

export const mutations = {
    addMessage(state, message){
        state.messages.unshift(message);
    },
    setMessages(state, messages) {
        state.messages = messages;
    }
}
